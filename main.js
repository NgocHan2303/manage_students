// thêm sv
// npx surge
// --- deploy

var dssv = [];
const DSSV_LOCAL = "DSSV_LOCAL";
// khi user load trang => lay du lieu tu localStorage

var jsonData = localStorage.getItem(DSSV_LOCAL);
if (jsonData != null) {
  // JSON.parse(jsonData) => array
  // convert array cũ ( lấy localStorage ) => không có key tinhDTB() => khi lưu xuống bị mất => khi lấy lên ko còn => convert thành array mới
  dssv = JSON.parse(jsonData).map(function (item) {
    // item : phần tử của array trong các lần lặp
    // return của map()
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
  });
  console.log("dssv: ", dssv);
  renderDSSV(dssv);
  // map js
}
console.log("jsonData: ", jsonData);

function themSV() {
  //   console.log({
  //     ma,
  //     ten,
  //     email,
  //     matKhau,
  //     toan,
  //     ly,
  //     hoa,
  //   });
  var sv = layThongTinTuForm();

  // kt mã
  var isValid =
    kiemTraTrung(sv.ma, dssv) && kiemTraDoDai(sv.ma, "spanMaSV", 4, 6);
  // kt email
  isValid = isValid & kiemTraEmail(sv.email);

  // kt mật khẩu
  // isValid = isValid & kiemTraDoDai(sv.matKhau, "spanMatKhau", 8, 20);

  // kt username
  isValid = isValid & kiemTraChuoi(sv.ten, "spanTenSV");
  // validate nhiều tiêu chí cho 1 input: &&
  // kết hợp validate giữa các ô input: &
  if (isValid) {
    //   push(): them phan tu vao array
    dssv.push(sv);
    //   console.log("themSV->dssv: ", dssv);

    // convert data
    let dataJson = JSON.stringify(dssv);
    // luu vao localStorage
    localStorage.setItem(DSSV_LOCAL, dataJson);
    // render dssv lên table
    renderDSSV(dssv);
    // tbodySinhVien
    resetForm();
  }
}
function xoaSV(id) {
  // splice: cut, slice: copy
  // console.log("xoaSV->id: ", id);
  var viTri = -1;
  for (var i = 0; i < dssv.length; i++) {
    if (dssv[i].ma == id) {
      viTri = i;
    }
  }
  if (viTri != -1) {
    // console.log("xoaSV->dssv before: ", dssv.length);
    dssv.splice(viTri, 1);
    // neu tim thay vi tri thi xoa
    // splice (vi tri, so luong)
    renderDSSV(dssv);
    // console.log("xoaSV->dssv after: ", dssv.length);
  }
  //   console.log("xoaSV->viTri: ", viTri);
}

function suaSV(id) {
  console.log("id: ", id);
  var viTri = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  console.log("suaSV->viTri: ", viTri);

  // show thong tin len form

  var sv = dssv[viTri];
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}
function capNhatSV() {
  // layThongTinTuForm() => return object sv

  var sv = layThongTinTuForm();
  console.log("capNhatSV->sv: ", sv);

  var viTri = dssv.findIndex(function (item) {
    return item.ma == sv.ma;
  });

  dssv[viTri] = sv;
  renderDSSV(dssv);
}
function resetForm() {
  document.getElementById("formQLSV").reset();
}
// 123: gia tri
// ('abc'): gia tri chuoi
// abc: bien

// splice, findIndex, map, push, forEach

// localStorage : luu tru, JSON : convert data
// notion
