function Cat(_name, _age) {
  this.name = _name;
  //   this.age = _age;
  this.tuoi = _age;
}

var cat1 = new Cat("miu", 1);
var cat2 = new Cat("mun", 2);
// console.log("cat1 : ", cat1);
// console.log("cat2 : ", cat2);

function SinhVien(ma, ten, email, matKhau, toan, ly, hoa) {
  this.ma = ma;
  this.ten = ten;
  this.email = email;
  this.matKhau = matKhau;
  this.toan = toan;
  this.ly = ly;
  this.hoa = hoa;
  this.tinhTDB = function () {
    return (this.toan * 1 + this.ly * 1 + this.hoa * 1) / 3;
  };
}
